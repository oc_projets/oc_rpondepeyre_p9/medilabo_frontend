import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, lastValueFrom } from 'rxjs';
import { Note } from 'src/app/models/Note';

export const GTWNote: string = 'http://localhost:7001/note';
export const GTWDiabete: string = 'http://localhost:7001/diabete';

@Injectable({
  providedIn: 'root',
})
export class NoteService {
  constructor(private http: HttpClient) {}

  notes: Note[] = [];

  diabeteRisk: string = '';

  async updateNotes(patientId: string) {
    this.notes = await lastValueFrom(this.findAllNotes(patientId));
    this.diabeteRisk = await lastValueFrom(
      this.calcDiabetebypatientId(patientId)
    );
  }

  findAllNotes(patientId: string): Observable<Note[]> {
    let param = new HttpParams().append('patientId', patientId);
    return this.http.get<Note[]>(GTWNote + '/all', { params: param });
  }

  addNote(value: Note): Observable<HttpResponse<string>> {
    return this.http.post<string>(GTWNote, value, {
      observe: 'response',
    });
  }

  deleteNote(id: number): Observable<HttpResponse<string>> {
    let param = new HttpParams().append('id', id);
    return this.http.delete<string>(GTWNote, {
      params: param,
      observe: 'response',
    });
  }

  calcDiabetebypatientId(id: string): Observable<string> {
    let param = new HttpParams().append('id', id);
    return this.http.get<string>(GTWDiabete, {
      params: param,
      responseType: 'text' as 'json',
    });
  }
}
