import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Patient } from 'src/app/models/Patient';

export const GTWPatient: string = 'http://localhost:7001/patient';

@Injectable({
  providedIn: 'root',
})
export class PatientService {
  constructor(private http: HttpClient) {}

  searchPatient(name: string): Observable<Patient[]> {
    let param = new HttpParams().append('name', name);
    return this.http.get<Patient[]>(GTWPatient + '/search', { params: param });
  }

  addPatient(value: Patient): Observable<HttpResponse<string>> {
    const patient: Patient = {
      ...value,
      datedenaissance: this.toUtc(value.datedenaissance),
    };
    return this.http.post<string>(GTWPatient, patient, {
      observe: 'response',
    });
  }

  editPatient(value: Patient): Observable<HttpResponse<string>> {
    const patient: Patient = {
      ...value,
      datedenaissance: this.toUtc(value.datedenaissance),
    };
    return this.http.put<string>(GTWPatient, patient, {
      observe: 'response',
    });
  }

  getPatient(id: number): Observable<Patient> {
    let param = new HttpParams().append('id', id);
    return this.http.get<Patient>(GTWPatient, { params: param });
  }

  toUtc(date: Date): Date {
    return new Date(
      Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0)
    );
  }
}
