import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, catchError, lastValueFrom } from 'rxjs';
import { AuthRequest } from 'src/app/models/AuthRequest';
import { UserAccount } from 'src/app/models/UserAccount';

export interface TokenResponse {
  token: string;
}
export const GTWAuth: string = 'http://localhost:7001/auth/';

@Injectable({
  providedIn: 'root',
})
export class AuthentService {
  private authenticatedUser: UserAccount;

  constructor(private http: HttpClient, private router: Router) {
    const storedUser = localStorage.getItem('authenticatedUser');
    if (storedUser) {
      this.authenticatedUser = JSON.parse(storedUser);
    }
  }

  login(request: AuthRequest): Observable<HttpResponse<TokenResponse>> {
    return this.http.post<TokenResponse>(GTWAuth + 'login', request, {
      observe: 'response',
    });
  }

  isAuthenticated(): boolean {
    return this.authenticatedUser !== undefined;
  }

  getAuthenticatedUser(): UserAccount {
    return this.authenticatedUser;
  }

  clearAuthentication() {
    this.authenticatedUser = undefined;
    localStorage.removeItem('token');
    localStorage.removeItem('authenticatedUser');
    this.router.navigate(['/login']);
  }

  async authenticate(request: AuthRequest) {
    let response: HttpResponse<TokenResponse> = await lastValueFrom(
      this.login(request).pipe(
        catchError((err: HttpErrorResponse) => {
          throw new Error(err.message);
        })
      )
    );
    if (response.body != null) {
      localStorage.setItem('token', response.body.token);
      this.authenticatedUser = await lastValueFrom(this.tokeninfo());
      localStorage.setItem(
        'authenticatedUser',
        JSON.stringify(this.authenticatedUser)
      );
      this.router.navigate(['home/search']);
    }
  }

  subscribe(subscriber: UserAccount): Observable<HttpResponse<string>> {
    return this.http.post<string>(GTWAuth + 'subscribe', subscriber, {
      observe: 'response',
    });
  }

  verifyMail(mail: string): Observable<boolean> {
    let param = new HttpParams().append('mail', mail);
    return this.http.get<boolean>(GTWAuth + 'mailexist', { params: param });
  }

  tokeninfo(): Observable<UserAccount> {
    return this.http.get<UserAccount>(GTWAuth + 'tokeninfo');
  }
}
