import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { UserAccount } from 'src/app/models/UserAccount';
import { AuthentService } from 'src/app/services/authentService/authent.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  get user(): UserAccount {
    return this.authService.getAuthenticatedUser();
  }
  constructor(
    private authService: AuthentService,
    private router: Router,
    private messageservice: MessageService
  ) {}

  logout() {
    this.authService.clearAuthentication();
    this.messageservice.add({
      severity: 'success',
      summary: 'Success',
      detail: 'Successfully logged out',
    });
  }
}
