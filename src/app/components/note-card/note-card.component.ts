import { HttpResponse } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { MessageService } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { Note } from 'src/app/models/Note';
import { NoteService } from 'src/app/services/noteService/note.service';

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss'],
})
export class NoteCardComponent {
  @Input() note: Note;

  constructor(
    private messageService: MessageService,
    private noteService: NoteService
  ) {}

  async deleteNote() {
    let response: HttpResponse<string> = await lastValueFrom(
      this.noteService.deleteNote(this.note._id)
    );
    if (response.status === 201) {
      this.noteService.updateNotes(this.note.patientId.toString());
      this.messageService.add({
        severity: 'success',
        summary: 'Success',
        detail: 'Note successfully deleted',
      });
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'ERROR',
        detail: 'An error occured, please try again',
      });
    }
  }
}
