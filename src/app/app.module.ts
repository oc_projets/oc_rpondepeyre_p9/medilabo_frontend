import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { NoteCardComponent } from './components/note-card/note-card.component';
import { PatientCardComponent } from './components/patient-card/patient-card.component';
import { AuthInterceptor } from './guards/auth-interceptor';
import { AuthentPageComponent } from './pages/authent-page/authent-page.component';
import { LoginPageComponent } from './pages/authent-page/login-page/login-page.component';
import { SubscribePageComponent } from './pages/authent-page/subscribe-page/subscribe-page.component';
import { AddPageComponent } from './pages/main-page/add-page/add-page.component';
import { EditPageComponent } from './pages/main-page/edit-page/edit-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PatientPageComponent } from './pages/main-page/patient-page/patient-page.component';
import { SearchPageComponent } from './pages/main-page/search-page/search-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainPageComponent,
    SearchPageComponent,
    AuthentPageComponent,
    LoginPageComponent,
    SubscribePageComponent,
    AddPageComponent,
    PatientCardComponent,
    PatientPageComponent,
    EditPageComponent,
    NoteCardComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    InputTextModule,
    ButtonModule,
    HttpClientModule,
    ToastModule,
    BrowserAnimationsModule,
    CalendarModule,
    RadioButtonModule,
    InputTextareaModule,
  ],
  providers: [
    ConfirmationService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
