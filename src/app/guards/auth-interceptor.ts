import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthentService } from '../services/authentService/authent.service';

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private messageservice: MessageService,
    private authentService: AuthentService
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let authReq = req.clone();
    let token = localStorage.getItem('token');
    if (authReq.url.includes('/auth') && !authReq.url.includes('/tokeninfo')) {
      return next.handle(authReq);
    } else {
      authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token),
      });
      return next.handle(authReq).pipe(
        catchError((error: HttpErrorResponse): Observable<any> => {
          if (error.status === 401) {
            this.authentService.clearAuthentication();
            this.messageservice.add({
              severity: 'error',
              summary: 'ERROR',
              detail: 'Your authentication has expired',
            });
            throw new Error('401: Your authentication has expired');
          } else {
            this.messageservice.add({
              severity: 'error',
              summary: 'ERROR',
              detail: error.error,
            });
            throw new Error(error.status + ': ' + error.error);
          }
        })
      );
    }
  }
}
