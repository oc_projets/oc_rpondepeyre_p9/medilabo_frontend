import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthentService } from '../services/authentService/authent.service';

export const loggedInGuard: CanActivateFn = (route, state) => {
  let authenticated: boolean = inject(AuthentService).isAuthenticated();
  if (authenticated) {
    return true;
  } else {
    inject(Router).navigate(['/auth/login']);
    return false;
  }
};
