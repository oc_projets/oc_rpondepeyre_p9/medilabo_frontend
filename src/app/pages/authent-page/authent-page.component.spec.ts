import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthentPageComponent } from './authent-page.component';

describe('AuthentPageComponent', () => {
  let component: AuthentPageComponent;
  let fixture: ComponentFixture<AuthentPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AuthentPageComponent]
    });
    fixture = TestBed.createComponent(AuthentPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
