import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { UserAccount } from 'src/app/models/UserAccount';
import { AuthentService } from 'src/app/services/authentService/authent.service';

@Component({
  selector: 'app-subscribe-page',
  templateUrl: './subscribe-page.component.html',
  styleUrls: ['./subscribe-page.component.scss'],
})
export class SubscribePageComponent {
  subscriber: UserAccount = {
    id: undefined,
    prenom: '',
    nom: '',
    mail: '',
    password: '',
  };

  passwordConfirmation: string = '';

  constructor(
    private authentService: AuthentService,
    private messageservice: MessageService,
    private routerService: Router
  ) {}

  showError(detail: string) {
    this.messageservice.add({
      severity: 'error',
      summary: 'ERROR',
      detail: detail,
    });
  }

  verifyFieldsCompletion(user: UserAccount) {
    return (
      user.prenom != '' &&
      user.nom != '' &&
      user.mail != '' &&
      user.password != ''
    );
  }

  async verifyMail(mail: string): Promise<boolean> {
    return lastValueFrom(this.authentService.verifyMail(mail));
  }

  async subscribe() {
    if (this.verifyFieldsCompletion(this.subscriber)) {
      if (this.subscriber.password === this.passwordConfirmation) {
        if (!(await this.verifyMail(this.subscriber.mail))) {
          let response: HttpResponse<string> = await lastValueFrom(
            this.authentService.subscribe(this.subscriber)
          );

          if (response.status === 201) {
            this.routerService.navigate(['auth/login']);
            this.messageservice.add({
              severity: 'success',
              summary: 'Success',
              detail: 'Account successfully created',
            });
          } else {
            this.showError('An error occured, please try again');
          }
        } else {
          this.showError('This mail address is already taken');
        }
      } else {
        this.showError("Passwords doesn't match");
      }
    } else {
      this.showError('All fields must be filled');
    }
  }
}
