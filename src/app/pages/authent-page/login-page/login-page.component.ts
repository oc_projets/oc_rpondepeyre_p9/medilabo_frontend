import { Component } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AuthRequest } from 'src/app/models/AuthRequest';
import { AuthentService } from 'src/app/services/authentService/authent.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent {
  authRequest: AuthRequest = {
    mail: '',
    password: '',
  };

  constructor(
    private authentService: AuthentService,
    private messageservice: MessageService
  ) {}

  async authenticate() {
    try {
      await this.authentService.authenticate(this.authRequest);
      this.messageservice.add({
        severity: 'success',
        summary: 'Success',
        detail: 'Successfully authenticated',
      });
    } catch (error) {
      this.messageservice.add({
        severity: 'error',
        summary: 'ERROR',
        detail: 'Invalid credentials',
      });
    }
  }
}
