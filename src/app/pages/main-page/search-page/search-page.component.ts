import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Patient } from 'src/app/models/Patient';
import { PatientService } from 'src/app/services/patientService/patient.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss'],
})
export class SearchPageComponent {
  constructor(private patientService: PatientService, private router: Router) {}

  patients?: Patient[] = undefined;

  async searchPatient(name: string) {
    let trimmedName = name.trim();
    if (trimmedName !== '') {
      this.patients = await lastValueFrom(
        this.patientService.searchPatient(trimmedName)
      );
    }
  }

  navigatePatient(id: number) {
    this.router.navigate(['/home/patient', id]);
  }
}
