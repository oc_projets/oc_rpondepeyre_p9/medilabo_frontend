import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { Patient } from 'src/app/models/Patient';
import { PatientService } from 'src/app/services/patientService/patient.service';

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.scss'],
})
export class AddPageComponent {
  patient: Patient = {
    id: undefined,
    nom: '',
    prenom: '',
    datedenaissance: undefined,
    genre: '',
    adresse: '',
    telephone: '',
  };

  address: string = '';
  postcode: string = '';
  city: string = '';

  constructor(
    private patientService: PatientService,
    private messageservice: MessageService,
    private routerService: Router
  ) {}

  showError(detail: string) {
    this.messageservice.add({
      severity: 'error',
      summary: 'ERROR',
      detail: detail,
    });
  }

  verifyFieldsCompletion(patient: Patient) {
    return (
      patient.id === undefined &&
      patient.nom !== '' &&
      patient.prenom !== '' &&
      patient.datedenaissance !== undefined &&
      patient.genre !== ''
    );
  }

  async addPatient() {
    let newPatient: Patient = this.patient;
    newPatient.adresse = this.address + ', ' + this.postcode + ', ' + this.city;

    if (this.verifyFieldsCompletion(newPatient)) {
      let response: HttpResponse<string> = await lastValueFrom(
        this.patientService.addPatient(newPatient)
      );

      if (response.status === 201) {
        this.routerService.navigate(['auth/login']);
        this.messageservice.add({
          severity: 'success',
          summary: 'Success',
          detail: 'Account successfully created',
        });
      } else {
        this.showError('An error occured, please try again');
      }
    } else {
      this.showError('All fields must be filled');
    }
  }
}
