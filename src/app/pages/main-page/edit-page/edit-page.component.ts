import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { Patient } from 'src/app/models/Patient';
import { PatientService } from 'src/app/services/patientService/patient.service';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
})
export class EditPageComponent implements OnInit {
  patient: Patient = {
    id: undefined,
    nom: '',
    prenom: '',
    datedenaissance: undefined,
    genre: '',
    adresse: '',
    telephone: '',
  };

  constructor(
    private route: ActivatedRoute,
    private patientService: PatientService,
    private routerService: Router,
    private messageService: MessageService
  ) {}

  async ngOnInit(): Promise<void> {
    let patientId = this.route.snapshot.params['id'];
    this.patient = await lastValueFrom(
      this.patientService.getPatient(patientId)
    );
  }

  showError(detail: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: detail,
    });
  }

  verifyFieldsCompletion(patient: Patient) {
    return (
      patient.nom !== '' &&
      patient.prenom !== '' &&
      patient.datedenaissance !== undefined &&
      patient.genre !== ''
    );
  }

  async editPatient() {
    let newPatient: Patient = this.patient;

    if (this.verifyFieldsCompletion(newPatient)) {
      let response: HttpResponse<string> = await lastValueFrom(
        this.patientService.editPatient(newPatient)
      );

      if (response.status === 201) {
        this.routerService.navigate(['home/patient', this.patient.id]);
        this.messageService.add({
          severity: 'success',
          summary: 'Success',
          detail: 'Patient successfully modified',
        });
      } else {
        this.showError('An error occured, please try again');
      }
    } else {
      this.showError('All fields must be filled');
    }
  }
}
