import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { differenceInYears } from 'date-fns';
import { MessageService } from 'primeng/api';
import { lastValueFrom } from 'rxjs';
import { Note } from 'src/app/models/Note';
import { Patient } from 'src/app/models/Patient';
import { UserAccount } from 'src/app/models/UserAccount';
import { NoteService } from 'src/app/services/noteService/note.service';
import { PatientService } from 'src/app/services/patientService/patient.service';

@Component({
  selector: 'app-patient-page',
  templateUrl: './patient-page.component.html',
  styleUrls: ['./patient-page.component.scss'],
})
export class PatientPageComponent implements OnInit {
  patient: Patient = {
    nom: '',
    prenom: '',
    datedenaissance: undefined,
    genre: '',
    adresse: '',
    telephone: '',
  };

  get notes(): Note[] {
    return this.noteService.notes;
  }

  set notes(notes: Note[]) {
    this.noteService.notes = notes;
  }

  get diabeteRisk(): string {
    return this.noteService.diabeteRisk;
  }

  set diabeteRisk(diabeteRisk: string) {
    this.noteService.diabeteRisk = diabeteRisk;
  }

  gender: string = undefined;
  age: number = undefined;
  newNotemessage: string = '';

  constructor(
    private route: ActivatedRoute,
    private patientService: PatientService,
    private noteService: NoteService,
    private messageService: MessageService
  ) {}

  async ngOnInit(): Promise<void> {
    let patientId = this.route.snapshot.params['id'];
    this.patient = await lastValueFrom(
      this.patientService.getPatient(patientId)
    );
    this.age = differenceInYears(
      new Date(),
      new Date(this.patient.datedenaissance)
    );
    this.gender = this.patient.genre === 'M' ? 'Male' : 'Female';

    //this.notes = await lastValueFrom(this.noteService.findAllNotes(patientId));

    this.noteService.updateNotes(patientId);
  }

  showError(detail: string) {
    this.messageService.add({
      severity: 'error',
      summary: 'ERROR',
      detail: detail,
    });
  }

  async addNote() {
    let note: Note = {
      patientId: 0,
      doctorName: '',
      postDateTime: undefined,
      message: '',
    };
    let user: UserAccount = JSON.parse(
      localStorage.getItem('authenticatedUser')
    ) as UserAccount;

    note.message = this.newNotemessage.trim();
    note.patientId = this.patient.id;
    note.doctorName = user.prenom + ' ' + user.nom;
    note.postDateTime = new Date();
    if (note.message.length > 0) {
      let response: HttpResponse<string> = await lastValueFrom(
        this.noteService.addNote(note)
      );

      if (response.status === 201) {
        this.noteService.updateNotes(this.route.snapshot.params['id']);
        this.messageService.add({
          severity: 'success',
          summary: 'Success',
          detail: 'Note successfully posted',
        });
      } else {
        this.showError('An error occured, please try again');
      }
    } else {
      this.showError('Your note is empty');
    }
  }
}
