export interface UserAccount {
  id?: number;
  nom: string;
  prenom: string;
  mail: string;
  password: string;
}
