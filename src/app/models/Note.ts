export interface Note {
  _id?: number;
  patientId: number;
  doctorName: string;
  postDateTime: Date;
  message: string;
}
