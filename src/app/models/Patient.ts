export interface Patient {
  id?: number;
  nom: string;
  prenom: string;
  datedenaissance: Date;
  genre: string;
  adresse: string;
  telephone: string;
}
