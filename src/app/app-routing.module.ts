import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { loggedInGuard } from './guards/logged-in.guard';
import { loggedOutGuard } from './guards/logged-out.guard';
import { AuthentPageComponent } from './pages/authent-page/authent-page.component';
import { LoginPageComponent } from './pages/authent-page/login-page/login-page.component';
import { SubscribePageComponent } from './pages/authent-page/subscribe-page/subscribe-page.component';
import { AddPageComponent } from './pages/main-page/add-page/add-page.component';
import { EditPageComponent } from './pages/main-page/edit-page/edit-page.component';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { PatientPageComponent } from './pages/main-page/patient-page/patient-page.component';
import { SearchPageComponent } from './pages/main-page/search-page/search-page.component';

const routes: Routes = [
  {
    path: 'home',
    component: MainPageComponent,
    canActivate: [loggedInGuard],
    children: [
      { path: 'search', component: SearchPageComponent },
      { path: 'addpatient', component: AddPageComponent },
      { path: 'patient/:id', component: PatientPageComponent },
      { path: 'edit/:id', component: EditPageComponent },
      { path: '**', redirectTo: 'search' },
    ],
  },
  {
    path: 'auth',
    component: AuthentPageComponent,
    canActivate: [loggedOutGuard],
    children: [
      { path: 'login', component: LoginPageComponent },
      { path: 'subscribe', component: SubscribePageComponent },
      { path: '**', redirectTo: 'login' },
    ],
  },
  { path: '**', redirectTo: 'auth' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
